TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := x64

TARGET_LINUX_IMAGE := bzImage

#HOST_CPP = x86_64-linux-gnu-gcc-6 -E
HOST_CPP = gcc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

ifdef PANTAVISOR_DISKCRYPT
TARGET_SKEL_DIRS := $(TOP_DIR)/vendor/x64-uefi/skels/diskcrypt
endif

